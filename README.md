# Rumbl.Umbrella

This is the resulting application from Programming Phoenix 1.4

I used `Elixir 1.12.3` and `Phoenix 1.6.2`

> If you are running on >= Phoenix 1.6, then you do not **have** to use node or webpack. Phoenix is now using esbuild. [See this article](https://fly.io/blog/phoenix-moves-to-esbuild-for-assets/).

## Issues you will run into on newer Phoenix versions:

### Chapter 9. Watching Videos

You will have some failing tests at the end of this chapter due to the removal of the `Listing Videos` heading. I unfortunately didn't catch this until I moved everything into the umbrella (Chapter 11).

### Chapter 10. Using Channels

You will need to generate the user socket with this command:

`$ mix phx.gen.socket User`

For more information, see [Phoenix Channels](https://hexdocs.pm/phoenix/channels.html#tying-it-all-together).

### Chapter 11. Observer and Umbrellas

I encountered an error after moving everything into the umbrella. This is the error I had and the fix that I used: https://elixirforum.com/t/pubsub-in-a-phoenix-app-under-umbrella/31083/14

### Chapter 13. Testing Channels and OTP

This [GitHub issue](https://github.com/phoenixframework/phoenix/issues/3619) documents the error that I encountered when trying to test the VideoChannel. This [comment](https://github.com/phoenixframework/phoenix/issues/3619#issuecomment-782728849) is the fix I used. You can see where I used it [here](https://gitlab.com/kevpo/rumbl/-/blob/main/apps/rumbl_web/test/channels/video_channel_test.exs#L11).
